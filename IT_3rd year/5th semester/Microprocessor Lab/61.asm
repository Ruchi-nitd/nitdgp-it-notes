mvi e, 02h
main:	dcr e
	lxi b, 0050h
	mvi h, 04h
	jnz loop

loop:	LDAX b
	mov l,a
	inx b
	LDAX b
	cmp l
	dcr h
	jc less
	jnz loop
	jz main

less:	mov d,a
	mov a,l
	stax b
	dcx b
	mov a,d
	stax b
	inx b
	jnz loop

hlt