#include<stdio.h>
#include<math.h>
#define ll long long int
#define phi (1+sqrt(5))/2.0
double pw(ll n){
  double r;
  if(n==0) return 1;
  r = pw(n/2);
  r = r*r;
  if(n%2) r = r*phi;
  return r;
}
int main()
{
	ll a,b;
	scanf("%lld",&a);
	b=pw(a)/sqrt(5)+0.5;
	printf("%lld\n",b);
	return 0;
}
