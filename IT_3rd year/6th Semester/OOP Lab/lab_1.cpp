#include<bits/stdc++.h>
using namespace std;

class BankAccount
{
public:
    string Name;
    string AcNo;
    string Type;
    string TypesName;
    double Balance;
    void init(string ANo, string HName, string AType, double Bal)
    {
        AcNo=ANo;
        Name=HName;
        Type=AType;
        Balance=Bal;
    }
    void Deposit(double Amount)
    {
        Balance+=Amount;
        cout << "New Balance = " << Balance << "\n";
    }
    void Withdrawal(double Amount)
    {
        if(Balance-Amount>1000)
        {
            Balance-=Amount;
            cout << "New Balance = " << Balance << "\n";
        }
        else cout << "Insufficient Balance\n";
    }
    void Display()
    {
        cout <<"Account No. = " << AcNo << "\n";
        cout <<"Account Holder's Name = " << Name << "\n";
        if(Type=="S") TypesName = "Savings";
        else if(Type=="C") TypesName = "Current";
        cout <<"Account Type = " << TypesName << "\n";
        cout<<"Balance = " << Balance << "\n";
    }

};

int main()
{
    string ANo, HName, AType;
    double Bal,Amount;
    int choice;
    BankAccount acc;
    printf("Enter the ANo, Holder's Name, AType and Initial Deposit(each in a separate line)\n");
    cin >> ANo;
    cin >> HName;
    cin >> AType;
    cin >> Bal;
    acc.init(ANo,HName,AType,Bal);
    printf("Enter 1 to deposit\n2 to withdraw\n3 to get all details\n0 to terminate\n");
    while(1)
    {
    scanf("%d",&choice);
    if(choice==1)
    {
        cout << "Enter Deposit Amt: ";
        cin >> Amount;
        acc.Deposit(Amount);
    }
    else if(choice==2)
    {
        cout << "Enter Withdrawal Amt: ";
        cin >> Amount;
        acc.Withdrawal(Amount);
    }
    else if(choice==3) acc.Display();
    else break;
    }
    return 0;
}
