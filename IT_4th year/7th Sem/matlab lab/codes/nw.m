function nw(a, supply, demand)
    b = zeros(3,3);
    row = 1;
    col = 1;
    cost = 0;
    while(row <= 3 && col <=4)
        sub = min(supply(row), demand(col));
        supply(row) = supply(row)-sub;
        demand(col) = demand(col)-sub;
        b(row,col) = sub;
        cost = cost + a(row, col)*sub;
        if(supply(row)==0)
            row = row + 1;
        else
            col = col + 1;
        end
    end
    disp('By North West Method');
    disp(b);
    disp(cost);
end