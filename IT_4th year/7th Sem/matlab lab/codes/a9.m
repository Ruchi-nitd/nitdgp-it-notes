clc;
a = [20,15,12,35;25,14,8,10;40,2,10,5;-5,4,11,0];
maxi = max(a);
maximin = min(maxi);
b = transpose(a);
mini = min(b);
minimax = max(mini);
if(minimax==maximin)
    row = find(mini==minimax);
    col = find(maxi==maximin);
    disp(row);
    disp(col);
    disp(minimax);
end