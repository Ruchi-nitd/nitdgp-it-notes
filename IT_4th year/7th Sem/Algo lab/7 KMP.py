#O(m+n)
def KMPSearch(pat, txt):
	M = len(pat)
	N = len(txt)
	j = 0
	lps = computeLPSArray(pat, M)
	i = 0
	found = 0
	while i < N:
		if pat[j] == txt[i]:
			j+=1
			i+=1
		if j == M:
			print("Found pattern at index "+ str(i-j))
			found = 1
			j = lps[j-1]
		elif i < N and pat[j] != txt[i]:
			if j != 0:
				j = lps[j-1]
			else:
				i = i+1
	if found == 0:
		print("Pattern not found")

def computeLPSArray(pat, M):
	lps = [0]*M
	l = 0
	i = 1
	while i < M:
		if pat[i] == pat[l]:
			l+=1
			lps[i] = l
			i+=1
		else:
			if l != 0:
				l = lps[l-1]
			else:
				lps[i] = 0
				i+=1
	return lps

txt = raw_input("Enter the text:\t")
pat = raw_input("Enter the pattern:\t")
KMPSearch(pat, txt)
